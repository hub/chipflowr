#' Provide R tools and an environment for the statistical analysis of ChIP-Seq data: load and clean data, produce figures, perform statistical analysis/testing with DESeq2 or edgeR, export results and create final report
#' @title Statistical Analysis of ChIP-Seq Tools
#' @author Maelle Daunesse, Marie-Agnes Dillies and Hugo Varet
#' @docType package
#' @name ChIPflow-package
NULL
