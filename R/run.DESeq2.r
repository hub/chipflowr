#' Wrapper to run DESeq2
#'
#' Wrapper to run DESeq2: create the \code{DESeqDataSet}, normalize data, estimate dispersions, statistical testing...
#'
#' @name run.DESeq2
#' @param counts \code{matrix} of raw counts
#' @param conditions \code{vector} listing the biological conditions (in the same order as the columns of counts)
#' @param batch \code{vector} of batch effect to take into account (\code{NULL} by default)
#' @param pAdjustMethod p-value adjustment method: \code{"BH"} (default) or \code{"BY"} for instance
#' @param spikes \code{vector} providing the size factors for each columns for spike-in normalisation (\code{NULL} by default)
#' @param ... optional arguments to be passed to \code{nbinomWaldTest()}
#' @return A list containing the \code{dds} object (\code{DESeqDataSet} class), the \code{results} objects (\code{DESeqResults} class) and the vector of size factors
#' @author Hugo Varet and Maëlle Daunesse
 
run.DESeq2 <- function(counts, conditions, batch=NULL, pAdjustMethod="BH", spikes=NULL, ...){
  # building dds object
  colData <- data.frame(conditions, row.names=colnames(counts))
  if (!is.null(batch)) colData$batch=batch
    
  dds <- DESeqDataSetFromMatrix(countData=counts, colData=colData, 
                                design=formula(paste("~", ifelse(!is.null(batch), paste("batch +"), ""), "conditions")))
  cat("Design of the statistical model:\n")
  cat(paste(as.character(design(dds)),collapse=" "),"\n")
 
  # normalization
  if(is.null(spikes)) {
    dds <- estimateSizeFactors(dds)
  } else {
    sizeFactors(dds) <- spikes/max(spikes)
  }
  cat("\nNormalization factors:\n")
  print(sizeFactors(dds))
  
  # estimating dispersions
  dds <- estimateDispersions(dds, fitType="parametric")
  
  # statistical testing: perform all the comparisons between the levels of design
  dds <- nbinomWaldTest(dds, ...)
  results <- list()
  for (comp in combn(nlevels(colData(dds)[, "conditions"]), 2, simplify=FALSE)){
    levelRef <- levels(colData(dds)[, "conditions"])[comp[1]]
    levelTest <- levels(colData(dds)[, "conditions"])[comp[2]]
    counts.norm <- as.data.frame(counts(dds, normalized=TRUE))
    colnames(counts.norm) <- paste0("norm.",colnames(counts))
    res <- results(dds, contrast=c("conditions", levelTest, levelRef),
                   pAdjustMethod=pAdjustMethod, cooksCutoff=TRUE, independentFiltering=FALSE)
    res <- res[, c("baseMean", "log2FoldChange", "lfcSE", "stat", "pvalue", "padj")]
    res$baseMean <- round(res$baseMean, 2)
    res$log2FoldChange <- round(res$log2FoldChange, 2)
    res$lfcSE <- round(res$lfcSE, 2)
    res$stat <- round(res$stat, 2)
    resAnDif <- data.frame(counts, counts.norm, res)
    results[[paste0(levelTest,"_vs_",levelRef)]] <- resAnDif
    cat(paste("Comparison", levelTest, "vs", levelRef, "done\n"))
  }
  return(list(dds=dds, results=results, sf=sizeFactors(dds), counts.trans=assay(vst(dds))))
}

