#' Wrapper to run Limma
#'
#' Wrapper to run Limma: ...
#'
#' @name run.Limma
#' @param counts \code{matrix} of raw counts
#' @param conditions \code{vector} listing the biological conditions (in the same order as the columns of counts)
#' @param normalize.method \code{"quantile"} (default) or \code{"scale"} or \code{"cyclicloess"} or \code{"none"} to choose a normalization method (force to "none" if spikes parameter is provided) 
#' @param spikes \code{vector} providing the size factors for each columns for spike-in normalisation (\code{NULL} by default)
#' @param pAdjustMethod p-value adjustment method: \code{"BH"} (default) or \code{"BY"} for instance
#' @param batch \code{vector} of batch effect to take into account (\code{NULL} by default)
#' @param outfile TRUE to export the figure in a png file
#' @param ... optional arguments to be passed to \code{voom()}
#' @return A list containing the \code{voom} object, the \code{results} objects
#' @author Maëlle Daunesse

run.Limma <- function(counts, conditions, normalize.method="quantile", spikes=NULL, 
                      pAdjustMethod="BH", batch=NULL, outfile=FALSE,...){
  # Building design
  if (is.null(batch)){
    design <- model.matrix(~ 0 + factor(conditions))
    colnames(design) <- levels(factor(conditions))  
  } else {
    design <- model.matrix(~ 0 + factor(conditions) + batch)
    colnames(design) <- c(levels(factor(conditions)), levels(factor(batch))[-1])
  }
  rownames(design) <- colnames(counts)
  
  # normalization and build of the voom object
  if (outfile) png(filename="meanVarTrend_Limma.png", width=1800, height=1600, res=300)
  if(is.null(spikes)) {
    v <- voom(counts=counts, design=design, normalize.method=normalize.method, 
              plot=FALSE, save.plot=TRUE)
  } else {
    
    v <- voom(counts = log(t((apply(counts,1,function(x) x/spikes)))+1), design=design, normalize.method="none", 
              plot=FALSE, save.plot=TRUE, ...)
    if(normalize.method != "none"){
      warning("normalize.method has been forced to 'none' as spikes normalization factors are provided.")
    }
  }
  if (outfile) dev.off()
  
  fit <- lmFit(object=v, design=design)
  # statistical testing: perform all the comparisons between the levels of design
  results <- list()
  for (comp in combn(levels(factor(conditions)), m=2, simplify=FALSE)){
    levelRef <- comp[1]
    levelTest <- comp[2]
    cont.matrix <- makeContrasts(contrasts=paste0(levelTest,"-",levelRef), levels=colnames(design))
    fit2 <- contrasts.fit(fit=fit, contrasts=cont.matrix)
    efit <- eBayes(fit=fit2)
    colnames(v$E) <- paste0("norm.", colnames(counts))
    resAnDif <- data.frame(counts, 
                           round(v$E, 2),
                           baseMean=round(efit$Amean, 2),
                           log2FoldChange=round(as.numeric(efit$coefficients), 2),
                           pvalue=as.numeric(efit$p.val),
                           padj=p.adjust(efit$p.value, method=pAdjustMethod))
    results[[paste0(levelTest,"_vs_",levelRef)]] <- resAnDif
    cat(paste("Comparison", levelTest, "vs", levelRef, "done\n"))
  }
  return(list(voom=v, results=results))
}

