#' Total number of reads per sample
#'
#' Bar plot of the total number of reads per sample
#'
#' @name barplotTotal
#' @param counts \code{matrix} of counts
#' @param conditions factor vector of the condition from which each sample belongs
#' @param col colors of the bars (one color per biological condition)
#' @param outfile TRUE to export the figure in a png file
#' @return A bar plot of the total number of reads per sample
#' @author Marie-Agnes Dillies, Hugo Varet and Maëlle Daunesse

barplotTotal <- function(counts, conditions, 
                         col=c("#f3c300", "#875692", "#f38400", "#a1caf1", "#be0032", "#c2b280", "#848482", "#008856", "#e68fac", "#0067a5", "#f99379", "#604e97"), 
                         outfile=FALSE){
  if (outfile) png(filename="BarplotTotal.png",width=min(3600,800+800*ncol(counts)),height=3500,res=300)
  d <- data.frame(tc=colSums(counts)/1e6, sample=factor(colnames(counts), colnames(counts)), conditions)
  print(ggplot(d, aes(x=.data$sample, y=.data$tc, fill=.data$conditions)) +
          geom_bar(stat="identity", show.legend=TRUE) +
          labs(fill="") +
          scale_fill_manual(values=col) +
          xlab("Samples") + 
          ylab("Total read count (million)") +
          scale_y_continuous(expand=expansion(mult=c(0.01, 0.05))) +
          ggtitle("Total read count per sample (million)") +
          theme_light() +
          theme(axis.text.x=element_text(angle=90, hjust=1, vjust=0.5)))
  if (outfile) dev.off()
}
