# ChIPflowR

How to install ChIPflowR?
-------------------------

In addition to the ChIPflowR package itself, the workflow requires the installation of several packages: DESeq2, limma, knitr... (all available online, see the dedicated webpages).

To install the ChIPflowR package from GitLab, open a R session and:
- Install devtools with `install.packages("devtools")` (if not installed yet)
- Notes:

	- Ubuntu users may have to install some libraries (libxml2-dev, libcurl4-openssl-dev and libssl-dev) to be able to install DESeq2 and devtools
	- Some users may have to install the pandoc and pandoc-citeproc libraries to be able to generate the final HTML reports

- For Windows users only, install [Rtools](https://cran.r-project.org/bin/windows/Rtools/) or check that it is already installed (needed to build the package)
- Run `devtools::install_gitlab(repo="hub/chipflowr", host="gitlab.pasteur.fr", build_vignettes=TRUE)`




ChIPflowR is part of https://gitlab.pasteur.fr/hub/epeak/
